# Cómo chambear en el sitio

## La idea general

Todos los archivos del sitio web estarán en un repositorio git en GitLab. Git es básicamente un software que ayuda a poder trabajar en código o texto entre varios usuarios y combinar el trabajo de todos. Nosotros podemos tener una copia de ese repositorio en nuestra computadora, y los cambios que hagamos a los archivos se pueden subir al repositorio, y así todo mundo puede tener la versión mas vigente de todo y permite tener también el control de las versiones de los archivos.

En general se pudiera hacer en google drive también. Pero lo bueno de acostumbrarnos a trabajarlos así en el repositorio es que ahí mismo en Gitlab se puede publicar la pagina. Entonces eventualmente ya que la publiquemos, solo tenemos que seguir trabajando en la pagina de la misma manera en que lo haríamos sin publicarla, sincronizar los cambios con el repositorio y ya, los cambios estarían publicados. La pagina publicada en Gitlab tendría una dirección relativamente corta e.g. farolito.gitlab.io , que se me hace relativamente bien para empezar. Si se quisiera se pudiera luego pagar por un dominio para tener una dirección mas corta, p ej. farolito.com, y pudiéramos seguir trabajándola de la misma manera.

La pagina se empezó a desarrollar usando un paquete de R llamado blogdown, que hace paginas de internet a su vez usa un software llamado Hugo y archivos de markdown (.md) o Rmarkdown (.Rmd). No se necesita tener el paquete instalado para contribuir a la pagina. Solo se ocupa para crear los archivos iniciales, vistas previas, o crear versiones de los archivos a publicar. La ventaja de hacerlo de esta manera es que las paginas del sitio se generan de manera muy sencilla, prácticamente escribiendo simplemente, sin necesidad de usar código, las paginas generadas son sencillas y rapidísimas de cargar por los navegadores de cualquier dispositivo (celular, compu nueva, compu viejísima, etc), y que solo estará en el sitio lo que nosotros queramos que este. Si se hiciera la pagina de otra manera, p ej. con Wix o Wordpress y esas cosas, aun paginas sencillas tardan mas en cargar y tienen muchos elementos de Internet de Babylon, p ej efectos innecesarios que solo hacen lento al sitio, publicidad, cookies que roben datos de los usuarios y que no podemos controlar nosotros como creadores del sitio.


## Requisitos

Para poder trabajar así se necesitan algunas cosas.
1. Tener instalado el software Git. Se puede descargar un instalador de Git para Windows o Mac de aqui [https://git-scm.com/downloads](https://git-scm.com/downloads) , o instalarlo desde cualquier repositorio de software en Linux.
2. Tener una cuenta de GitLab[https://git-scm.com/download/mac](https://git-scm.com/download/mac), y ser miembro del repositorio de Farolito. Hinojo puede agregar miembros al repositorio.
3. Tener un editor archivos de puro texto, p ej. notepad (Windows), text editor (Mac), atom, nano, vim, emacs, etc. Decisión 1000% personal segun su preferencia.


## Configurando Git en tu compu

Normalmente Git es una aplicación de la terminal, pero también tiene una versión gráfica. Yo (Hinojo) solo he usado la versión de la terminal y esa explicare. El primer paso es configurar unas cosas básicas, como nuestro Nombre, correo, y otras cosas. Esto solo se corre una vez por compu.

Se configura el nombre y el correo corriendo esto en la terminal (pon tus datos donde se necesite):

	git config --global user.name "Pedro Perez"
	git config --global user.email "pedroperez@email.com"

Correr esta otra cosa también tal cual según su sistema operativo. Para Windows:

	git config --global core.autocrlf true

O en Mac y Linux correr esto:

	git config --global core.autocrlf input

Y configurar su editor de texto aquí... Esto no es tan importante pero en teoría hay que hacerlo. En la mayoría de los casos se le tiene que agregar -w después del nombre del editor, pero a algunos editores de la terminal no. Por ejemplo poner esto para atom o nano:
	
	git config --global core.editor "atom -w"
	git config --global core.editor "nano -w"

Ver [aqui](https://swcarpentry.github.io/git-novice/02-setup/index.html) otros ejemplos para otros editores de texto.


## Trabajando en el sitio

Ya para empezar a trabajar en la pagina primero debemos tener una copia del repositorio en nuestra compu. Para eso, abre una terminal, vete al directorio donde lo quieras y abre la terminal ahí o llega a el usando "cd", p ej.
	
	cd ~/Documentos

Ya en el directorio, para descargar el repositorio corre esto
	
	git clone https://gitlab.com/farolito/farolito.gitlab.io.git

Eso creara una carpeta con todos los archivos del sito web y ya puedes comenzar a editarlos en tu editor de texto.

Siempre que vayas a trabajar en el sitio web sigue estos pasos, en la terminal, dentro del directorio del repositorio de farolito:

**Paso 1**. Asegúrate primero de tener la versión mas actual del repositorio con esto. Se le llama hacer pull. Te pedirá tu usuario y contraseña de Gitlab y se descargaran las cosas nuevas que haya.
	
	git pull origin master


**Paso 2**. Cuando termines de trabajar, o cuando termines alguna tarea especifica que estabas haciendo del sitio, se debe actualizar el repositorio en linea con tus cambios. Para esto primero señala que archivos cambiaste así, por ejemplo:
	
	git add volumenes.md farolito_vol3.pdf

Si modificaste muchos muchos archivos y te da weba seleccionarlos todos, puedes correr esto para señalarlos a todos:
	
	git add .

**Paso 3**. Dile que registre los cambios así. Debes poner un mensaje breve, por ejemplo:
	
	git commit -m "Agregue el tercer volumen y sube su pdf"

**Paso 4**. Y sube los cambios, así. Te pedirá usuario y contraseña de Gitlab.

	git push origin master

En general cada que trabajas, debes checar que el repositorio en tu compu este actualizado, trabajar en algunos archivos, registrar los cambios y sincronizar, es decir, hacer los pasos 1-4.


## Estructura de los archivos en el sitio

En la carpeta *content* están los archivos .md y .Rmd que se convertirán en las paginas individuales del sitio. Estos son los archivos que debes editar para editar el contenido de las paginas del sitio web. Dentro de *content*, hay una carpeta llamada *post*, que ahí se pueden poner entradas de un blog, y deje un archivo de una entrada como ejemplo, aunque no aparece en el sitio web, es solo para tenerlo de referencia.

En la carpeta *static* irán los archivos diversos usados en las diferentes paginas del sitio, como imágenes, pdfs, etc. Dentro de *static*, hice una carpeta para poner las imagenes y otra para pdfs.


La carpeta *public* tiene los archivos que realmente componen al sitio, y que son los archivos que se interpretaran como el sitio web, y pues, son los que abren los navegadores. Si los tratas de abrir con un navegador se verán feos, porque están hechos para verse correctamente en Internet.

Algunos elementos del sitio se pueden configurar en el archivo *config.toml*. Este archivo no esta dentro de ninguna carpeta, esta suelto en el folder del repositorio. Aquí se puede configurar el menú, el logo, los links del facebook, instagram, email, y otras cosas. Debe ser relativamente sencillo de entender, aunque tiene muchas mamadas que no se usan en el sitio. Eventualmente dejare solo las cosas que el sitio usa realmente.
	

## Nota sobre los archivos Markdown (.md o .Rmd)

Los archivos Markdown son un tipo de formato para describir el contenido de un documento. Llevan un encabezado

	---
	title: titulo
	---

El texto solo se escribe como texto, y los diferentes párrafos están separados un renglón. Los encabezados se agregan con #, p ej.
	
	Parrafo 1

	Parrafo 2

	# Este es un encabezado

	## Este es un subencabezado

Se le puede dar algo de formato al texto, p ej.
	
	**Esto da negritas**
	*Esto da cursivas*

Los hiper-vínculos se ponen así:
	
	[texto que aparecera](direccion de internet)

Las imágenes se agregan así

	![](/donde/esta/el/archivo.png)

O controlar el tamaño de la imagen en proporción a la pantalla (solo funciona en .Rmd). Recomiendo siempre usar archivos .Rmd.

	![](/donde/esta/el/archivo.png){width=30%}


## Nota sobre el paquete blogdown

Si no tienes el paquete instalado, instálalo en R asi:
	
	install.packages("blogdown")
	library(blogdown)
	install_hugo()

El sitio usa el tema [KeepIt](https://themes.gohugo.io/keepit/), que usa un formato sencillo de todo. Si se quisieran cosas mas especificas habría que modificarle cosas mas frikimente. Por ahora hay que tratar de mantenernos en formatos simples, y si queremos algo muy particular, lo vamos viendo poco a poco. Se pudieran usar otros temas, pero hay otros que son tan específicos que limitan mas que ayudar y otros son tan flexibles que son muy complicados de usar, y no todos los contenidos son perfectamente compatibles entre un tema u otro (p ej. cada tema tiene su manera particular de hacer ciertas cosas como el menú, los componentes de la pagina, etc). Lo que si es compatible es la manera de hacer los markdown (como agregar texto, encabezados, formato de texto, links, imágenes, etc). [Aqui](https://themes.gohugo.io/) vienen otros temas que se pudieran usar. En lo personal (Hinojo) me gusto bastante el que ya esta (KeepIt).

Normalmente solo usarías R y este paquete para tener vistas previas del sitio web. Para esto solo necesitas poner como directorio de trabajo el directorio donde tienes el repositorio del sitio en tu computadora, y correr estas lineas en R:

	library(blogdown)
	serve_site()
